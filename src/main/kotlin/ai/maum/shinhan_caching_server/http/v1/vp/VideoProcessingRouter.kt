package ai.maum.shinhan_caching_server.http.v1.vp

import ai.maum.shinhan_caching_server.http.util.alias.ContentType
import ai.maum.shinhan_caching_server.http.v1.vp.report.CreateReportHandler
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.function.server.router

@Configuration
class VideoProcessingRouter(
    private val vpCreateReportHandler: CreateReportHandler
) {
    @Bean
    fun vpRoute() = router {
        "/caching/v1/vp".nest {
            "/report".nest {
                POST(
                    "/create",
                    ContentType.REQUEST_JSON and ContentType.RESPONSE_JSON,
                    vpCreateReportHandler::reportVideo
                )
            }
        }
    }
}
