package ai.maum.shinhan_caching_server.http.v1.api.video.dto

import kotlinx.serialization.Serializable

@Serializable
data class CreateVideoToApiRequest(
    var file: String,
    var size: Long,
    var result: String
)
