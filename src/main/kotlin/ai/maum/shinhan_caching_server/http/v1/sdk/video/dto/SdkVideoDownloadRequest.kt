package ai.maum.shinhan_caching_server.http.v1.sdk.video.dto

import kotlinx.serialization.Serializable

@Serializable
data class SdkVideoDownloadRequest(
    var video: Video
) {
    @Serializable
    data class Video(
        var name: String
    )
}
