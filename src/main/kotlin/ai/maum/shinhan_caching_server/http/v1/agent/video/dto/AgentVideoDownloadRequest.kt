package ai.maum.shinhan_caching_server.http.v1.agent.video.dto

import kotlinx.serialization.Serializable

@Serializable
data class AgentVideoDownloadRequest(
    var video: Video
) {
    @Serializable
    data class Video(
        var name: String
    )
}
