package ai.maum.shinhan_caching_server.http.v1.api.video.dto

import kotlinx.serialization.Serializable

@Serializable
data class CreateVideoLiveRequest(
    var avatar: String,
    var outfit: String,
    var background: Background,
    var utter: String,
    var resolution: Resolution,
    var gesture: String,
    var scenarioVersion: String,
    var engine: Engine
) {
    @Serializable
    data class Background(
        var path: String
    )

    @Serializable
    data class Resolution(
        var name: String,
        var width: Int,
        var height: Int
    )

    @Serializable
    data class Engine(
        var ttsHost: String,
        var ttsPort: Int,
        var ttsPath: String,
        var w2lHost: String,
        var w2lPort: Int,
        var w2lPath: String
    )
}
