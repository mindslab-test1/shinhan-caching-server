package ai.maum.shinhan_caching_server.http.v1.agent.video

import ai.maum.shinhan_caching_server.http.v1.agent.video.dto.AgentVideoDownloadRequest
import ai.maum.shinhan_caching_server.util.logger.DslLogger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.ByteArrayResource
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse.ok
import org.springframework.web.reactive.function.server.body
import org.springframework.web.reactive.function.server.bodyToMono
import org.springframework.web.server.ResponseStatusException
import reactor.core.publisher.Mono
import java.io.File

@Component
class AgentVideoHandler(
    @Value("\${ai.maum.shinhan.caching_server.directory.root}")
    private val dataRootDirectory: String,
    @Value("\${ai.maum.shinhan.caching_server.location.video}")
    private val videoLocation: String,
) {
    private val log = DslLogger(LoggerFactory.getLogger(this::class.java))

    fun downloadVideoForAgent(request: ServerRequest) = request.bodyToMono<AgentVideoDownloadRequest>()
        .flatMap { (video) ->
            val branchNo = request.headers().header("branchNo")
            val deviceNo = request.headers().header("deviceNo")
            val videoName = video.name.replace(".mp4", "") + ".mp4"

            log info "[ $branchNo:$deviceNo ] Agent downloading $videoName"
            val file = File("$dataRootDirectory$videoLocation/$videoName")

            file.let {
                if (!file.exists()) {
                    log error "[ $branchNo:$deviceNo ] Failed to find video $videoName"
                    throw ResponseStatusException(HttpStatus.NOT_FOUND, "requested file not found")
                }

                ok().contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .body(
                        Mono.just(object : ByteArrayResource(file.inputStream().readAllBytes()) {
                            override fun getFilename(): String? = file.name
                        })
                    )
            }
        }
}
