package ai.maum.shinhan_caching_server.http.v1.api

import ai.maum.shinhan_caching_server.http.util.alias.ContentType.Companion.REQUEST_JSON
import ai.maum.shinhan_caching_server.http.util.alias.ContentType.Companion.RESPONSE_JSON
import ai.maum.shinhan_caching_server.http.v1.api.video.CreateVideoHandler
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.function.server.router

@Configuration
class ApiRouter(
    private val createVideoHandler: CreateVideoHandler
) {
    @Bean
    fun apiRoute() = router {
        "/caching/v1/api".nest {
            "/create".nest {
                POST("/request", REQUEST_JSON and RESPONSE_JSON, createVideoHandler::createVideo)
                POST("/live", REQUEST_JSON and RESPONSE_JSON, createVideoHandler::createVideoLive)
            }
        }
    }
}
