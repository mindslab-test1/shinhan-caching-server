package ai.maum.shinhan_caching_server.http.v1.sdk.video

import ai.maum.shinhan_caching_server.http.v1.sdk.video.dto.SdkVideoDownloadRequest
import ai.maum.shinhan_caching_server.util.logger.DslLogger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.ByteArrayResource
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.body
import org.springframework.web.server.ResponseStatusException
import reactor.core.publisher.Mono
import java.io.File

@Component
class SdkVideoHandler {
    private val log = DslLogger(LoggerFactory.getLogger(this::class.java))

    @Value("\${ai.maum.shinhan.caching_server.directory.root}")
    lateinit var dataRootDirectory: String

    @Value("\${ai.maum.shinhan.caching_server.location.video}")
    lateinit var videoLocation: String

    fun downloadVideoForSdk(request: ServerRequest) = request.bodyToMono(SdkVideoDownloadRequest::class.java)
        .flatMap { (video) ->
            val branchNo = request.headers().header("branchNo")
            val deviceNo = request.headers().header("deviceNo")

            val videoName = video.name.replace(".mp4", "") + ".mp4"
            val file = File("$dataRootDirectory$videoLocation/$videoName")

            log info "[ $branchNo:$deviceNo ] Download request from sdk: $videoName"

            if (!file.exists()) {
                log error "[ $branchNo:$deviceNo ] Failed to find video: $videoName"
                return@flatMap Mono.error(ResponseStatusException(HttpStatus.NOT_FOUND, "requested file not found"))
            }

            return@flatMap ServerResponse.ok()
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(
                    Mono.just(
                        let {
                            log info "[ $branchNo:$deviceNo ] Responding video stream: $videoName"

                            return@let object : ByteArrayResource(file.inputStream().readAllBytes()) {
                                override fun getFilename(): String? = file.name
                            }
                        }
                    )
                )
        }
}
