package ai.maum.shinhan_caching_server.http.v1.vp.report.dto

import kotlinx.serialization.Serializable

@Serializable
data class CreateVideoReportFromVpRequest(
    var name: String,
    var liveYn: String
)
