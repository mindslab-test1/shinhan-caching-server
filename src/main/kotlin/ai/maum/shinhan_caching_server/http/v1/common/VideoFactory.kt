package ai.maum.shinhan_caching_server.http.v1.common

import ai.maum.shinhan_caching_server.http.exception.CreateTargetSizeZeroException
import ai.maum.shinhan_caching_server.http.v1.api.video.dto.CreateVideoToApiRequest
import ai.maum.shinhan_caching_server.http.v1.api.video.dto.CreateVideoToVpRequest
import ai.maum.shinhan_caching_server.r2dbc.entity.Video
import ai.maum.shinhan_caching_server.r2dbc.repository.VideoRepository
import ai.maum.shinhan_caching_server.util.logger.DslLogger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.MediaType
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.bodyToMono
import reactor.core.publisher.Mono

@Component
class VideoFactory(
    private val apiServerWebClient: WebClient,
    private val videoProcessingServerWebClient: WebClient,
    private val videoRepository: VideoRepository
) {
    private val log = DslLogger(LoggerFactory.getLogger(this::class.java))

    @Value("\${create.capacity}")
    private var createCapacity: Int = 0

    fun processForCreateVideo() = videoRepository.countByLiveYnAndStatus("N", 1)
        .flatMap { workingCnt ->
            val reqCnt = createCapacity - workingCnt
            log info "요청 가능한 개수: $reqCnt"

            if (reqCnt == 0) Mono.error(CreateTargetSizeZeroException())
            else videoRepository.findCreateTargets(reqCnt)
                .collectList()
                .flatMap { targets ->
                    if (targets.size == 0) Mono.error(CreateTargetSizeZeroException())
                    else {
                        val files = targets.stream().map { it.file }.toList() as List<String>
                        log debug "요청 대기 중인 영상: $files"
                        videoRepository.updateStatusProgressByFiles(files, files, targets.size)
                            .doOnSuccess { updatedCnt ->
                                if (updatedCnt > 0)
                                    targets.forEach { target ->
                                        log info "Vp로 영상 생성 요청: ${target.file}"
                                        requestToVp(createRequestDto(target))
                                    }
                            }
                    }
                }
        }

    fun reportToApi(body: CreateVideoToApiRequest) =
        apiServerWebClient
            .post()
            .uri("/api/v1/report/create")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(body)
            .retrieve()
            .bodyToMono<Void>()
            .subscribe()

    fun requestToVp(body: CreateVideoToVpRequest) =
        videoProcessingServerWebClient
            .post()
            .uri("/vp/v1/caching/video/synthesize")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(body)
            .retrieve()
            .bodyToMono<Void>()
            .subscribe()

    fun createRequestDto(target: Video) = CreateVideoToVpRequest(
        target.file!!,
        CreateVideoToVpRequest.Background(target.background!!),
        target.utter!!,
        CreateVideoToVpRequest.Resolution(target.width!!, target.height!!),
        target.gesture!!,
        CreateVideoToVpRequest.Engine(
            target.ttsHost!!,
            target.ttsPort!!,
            target.ttsPath!!,
            target.w2lHost!!,
            target.w2lPort!!,
            target.w2lPath!!
        ),
        target.liveYn!!
    )
}
