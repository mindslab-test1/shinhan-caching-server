package ai.maum.shinhan_caching_server.http.v1.agent

import ai.maum.shinhan_caching_server.http.util.alias.ContentType.Companion.REQUEST_JSON
import ai.maum.shinhan_caching_server.http.util.alias.ContentType.Companion.RESPONSE_OCTET
import ai.maum.shinhan_caching_server.http.v1.agent.video.AgentVideoHandler
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.function.server.router

@Configuration
class AgentRouter(
    private val agentVideoHandler: AgentVideoHandler
) {
    @Bean
    fun agentRoute() = router {
        "/caching/v1/agent".nest {
            POST("/video", REQUEST_JSON and RESPONSE_OCTET, agentVideoHandler::downloadVideoForAgent)
        }
    }
}
