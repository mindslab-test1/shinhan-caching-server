package ai.maum.shinhan_caching_server.http.v1.sync.dto

import kotlinx.serialization.Serializable

@Serializable
data class SyncVideoDownloadRequest(
    var video: Video
) {
    @Serializable
    data class Video(
        var name: String
    )
}
