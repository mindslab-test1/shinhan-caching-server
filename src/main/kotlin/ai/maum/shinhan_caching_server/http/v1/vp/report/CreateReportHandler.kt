package ai.maum.shinhan_caching_server.http.v1.vp.report

import ai.maum.shinhan_caching_server.http.exception.VideoStatusUpdateFailException
import ai.maum.shinhan_caching_server.http.util.alias.CreateVideoOption.Companion.CREATE_REQUEST_RETRY_MAX_ATTEMPTS
import ai.maum.shinhan_caching_server.http.util.alias.CreateVideoOption.Companion.CREATE_REQUEST_RETRY_MIN_BACKOFF
import ai.maum.shinhan_caching_server.http.v1.api.video.dto.CreateVideoToApiRequest
import ai.maum.shinhan_caching_server.http.v1.common.VideoFactory
import ai.maum.shinhan_caching_server.http.v1.vp.report.dto.CreateVideoReportFromVpRequest
import ai.maum.shinhan_caching_server.r2dbc.repository.VideoRepository
import ai.maum.shinhan_caching_server.util.logger.DslLogger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.bodyToMono
import reactor.core.publisher.Mono
import reactor.util.retry.Retry
import java.nio.file.Files
import java.nio.file.Paths
import java.time.Duration

@Component
class CreateReportHandler(
    private val videoFactory: VideoFactory,
    private val videoRepository: VideoRepository
) {
    private val log = DslLogger(LoggerFactory.getLogger(this::class.java))

    @Value("\${ai.maum.shinhan.caching_server.directory.root}")
    lateinit var cachingRootDirectory: String
    @Value("\${ai.maum.shinhan.caching_server.location.video}")
    lateinit var cachingVideoDirectory: String
    @Value("\${ai.maum.shinhan.video_processing_server.directory.root}")
    lateinit var vpRootDirectory: String
    @Value("\${ai.maum.shinhan.video_processing_server.location.video}")
    lateinit var vpSynthesizedDirectory: String

    fun reportVideo(request: ServerRequest) = request.bodyToMono<CreateVideoReportFromVpRequest>()
        .flatMap { req ->
            val videoName = req.name
                .replace("\n", "")
                .replace("\r", "")
                .replace(".mp4", "") + ".mp4"

            log info "Vp로부터 영상 생성 결과: $videoName"

            videoRepository.updateStatusCompleteByFile(videoName)
                .flatMap { updatedCnt ->
                    if (updatedCnt == 0) Mono.error(VideoStatusUpdateFailException())
                    else {
                        Files.move(
                            Paths.get("$vpRootDirectory$vpSynthesizedDirectory/${req.name}"),
                            Paths.get("$cachingRootDirectory$cachingVideoDirectory/$videoName")
                        )
                        val fileSize = Files.size(Paths.get("$cachingRootDirectory$cachingVideoDirectory/$videoName"))

                        log info "Vp에서 Caching으로 영상 파일 이동: $videoName($fileSize)"

                        if (req.liveYn == "Y") Mono.empty()
                        else {
                            videoFactory.reportToApi(CreateVideoToApiRequest(videoName, fileSize, "result"))
                            videoFactory.processForCreateVideo()
                                .retryWhen(
                                    Retry.backoff(
                                        CREATE_REQUEST_RETRY_MAX_ATTEMPTS,
                                        Duration.ofSeconds(CREATE_REQUEST_RETRY_MIN_BACKOFF)
                                    )
                                )
                        }
                    }
                }.onErrorResume {
                    when (it.javaClass.simpleName) {
                        "FileAlreadyExistsException" -> log error "이미 존재하는 파일"
                        "NoSuchFileException" -> log error "파일 없음"
                        "VideoStatusUpdateFailException", "CreateTargetSizeZeroException" ->
                            log warn "${it.message}"
                        "RetryExhaustedException" ->
                            log warn "재요청 실패: $CREATE_REQUEST_RETRY_MAX_ATTEMPTS/$CREATE_REQUEST_RETRY_MAX_ATTEMPTS"
                        else -> log error "${it.message}"
                    }
                    Mono.empty()
                }
                .subscribe()

            ServerResponse.ok().build()
        }
}
