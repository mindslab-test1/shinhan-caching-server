package ai.maum.shinhan_caching_server.http.v1.sync

import ai.maum.shinhan_caching_server.http.v1.sync.dto.SyncVideoDownloadRequest
import ai.maum.shinhan_caching_server.util.logger.DslLogger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.ByteArrayResource
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException
import java.io.File

@RestController
class AgentSyncController {
    private val log = DslLogger(LoggerFactory.getLogger(this::class.java))

    @Value("\${ai.maum.shinhan.caching_server.directory.root}")
    lateinit var dataRootDirectory: String

    @Value("\${ai.maum.shinhan.caching_server.location.video}")
    lateinit var videoLocation: String

    @Suppress("ThrowsCount")
    @PostMapping("/caching/v1/sync/download/video")
    fun downloadVideoForAgentBySync(
        @RequestHeader branchNo: String,
        @RequestHeader deviceNo: String,
        @RequestBody body: SyncVideoDownloadRequest
    ): ResponseEntity<ByteArray> {
        val video = body.video
        val videoName = video.name.replace(".mp4", "") + ".mp4"
        val file = File("$dataRootDirectory$videoLocation/$videoName")

        log info "[ $branchNo:$deviceNo ] Sync video download request: ${video.name}"

        if (!file.exists()) {
            log error "[ $branchNo:$deviceNo ] Could not find requested video file: " +
                "$dataRootDirectory$videoLocation/$videoName"
            throw ResponseStatusException(HttpStatus.NOT_FOUND, "requested video file not found")
        }

        log debug "[ $branchNo:$deviceNo ] Reading bytearray from file: ${video.name}"
        val res = object : ByteArrayResource(file.inputStream().readAllBytes()) {
            override fun getFilename(): String? = file.name
        }

        log info "[ $branchNo:$deviceNo ] Responding vdieo stream: ${video.name}"
        return ResponseEntity.ok()
            .contentType(MediaType.APPLICATION_OCTET_STREAM)
            .body(res.byteArray)
    }
}
