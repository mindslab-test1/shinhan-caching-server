package ai.maum.shinhan_caching_server.http.v1.sdk

import ai.maum.shinhan_caching_server.http.util.alias.ContentType.Companion.REQUEST_JSON
import ai.maum.shinhan_caching_server.http.util.alias.ContentType.Companion.RESPONSE_OCTET
import ai.maum.shinhan_caching_server.http.v1.sdk.video.SdkVideoHandler
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.function.server.router

@Configuration
class SdkRouter(
    private val sdkVideoHandler: SdkVideoHandler
) {
    @Bean
    fun sdkRoute() = router {
        "/caching/v1/sdk".nest {
            POST("/video", REQUEST_JSON and RESPONSE_OCTET, sdkVideoHandler::downloadVideoForSdk)
        }
    }
}
