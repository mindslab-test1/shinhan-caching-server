package ai.maum.shinhan_caching_server.http.v1.api.video.dto

import kotlinx.serialization.Serializable

@Serializable
data class CreateVideoToApiResponse(
    var fileList: List<File>
) {
    @Serializable
    data class File(
        var name: String
    )
}
