package ai.maum.shinhan_caching_server.http.v1.api.video

import ai.maum.shinhan_caching_server.http.v1.api.video.dto.*
import ai.maum.shinhan_caching_server.http.v1.common.VideoFactory
import ai.maum.shinhan_caching_server.r2dbc.entity.Video
import ai.maum.shinhan_caching_server.r2dbc.repository.VideoRepository
import ai.maum.shinhan_caching_server.util.logger.DslLogger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.bodyToMono
import reactor.core.publisher.Mono
import java.util.*

@Component
class CreateVideoHandler(
    private val videoFactory: VideoFactory,
    private val videoRepository: VideoRepository
) {
    private val log = DslLogger(LoggerFactory.getLogger(this::class.java))

    fun createVideo(request: ServerRequest) = request.bodyToMono<CreateVideoFromApiRequest>()
        .flatMapMany { req ->
            log info "Api로부터 생성 요청된 영상: $req"

            videoRepository.saveAll(
                req.videos.stream().map {
                    Video().apply {
                        this.avatar = it.avatar
                        this.outfit = it.outfit
                        this.background = it.background.path
                        this.utter = it.utter
                        this.resolution = it.resolution.name
                        this.width = it.resolution.width
                        this.height = it.resolution.height
                        this.gesture = it.gesture
                        this.scenarioVersion = it.scenarioVersion
                        this.ttsHost = it.engine.ttsHost
                        this.ttsPort = it.engine.ttsPort
                        this.ttsPath = it.engine.ttsPath
                        this.w2lHost = it.engine.w2lHost
                        this.w2lPort = it.engine.w2lPort
                        this.w2lPath = it.engine.w2lPath
                        this.file = UUID.randomUUID().toString() + ".mp4"
                    }
                }.toList()
            )
        }.doOnNext { video ->
            log info "요청 목록에 저장된 영상: ${video.file}"
        }.collectList()
        .flatMap { videos ->
            videoFactory.processForCreateVideo()
                .onErrorResume {
                    when (it.javaClass.simpleName) {
                        "CreateTargetSizeZeroException" -> log warn "${it.message}"
                        else -> log error "${it.message}"
                    }
                    Mono.empty()
                }.subscribe()

            ServerResponse.ok().bodyValue(
                CreateVideoToApiResponse(
                    videos.stream().map {
                        CreateVideoToApiResponse.File(it.file!!)
                    }.toList()
                )
            )
        }

    fun createVideoLive(request: ServerRequest) = request.bodyToMono<CreateVideoLiveRequest>()
        .flatMap { req ->
            videoRepository.save(
                Video().apply {
                    this.avatar = req.avatar
                    this.outfit = req.outfit
                    this.background = req.background.path
                    this.utter = req.utter
                    this.resolution = req.resolution.name
                    this.width = req.resolution.width
                    this.height = req.resolution.height
                    this.gesture = req.gesture
                    this.scenarioVersion = req.scenarioVersion
                    this.ttsHost = req.engine.ttsHost
                    this.ttsPort = req.engine.ttsPort
                    this.ttsPath = req.engine.ttsPath
                    this.w2lHost = req.engine.w2lHost
                    this.w2lPort = req.engine.w2lPort
                    this.w2lPath = req.engine.w2lPath
                    this.liveYn = "Y"
                    this.file = UUID.randomUUID().toString() + ".mp4"
                    this.status = 1
                }
            )
        }.flatMap { video ->
            log info "Vp로 실시간 발화 영상 생성 요청: ${video.file}"
            videoFactory.requestToVp(videoFactory.createRequestDto(video))
            ServerResponse.ok().bodyValue(CreateVideoLiveResponse(CreateVideoLiveResponse.File(video.file!!)))
        }
}
