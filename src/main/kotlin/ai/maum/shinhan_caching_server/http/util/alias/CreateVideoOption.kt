package ai.maum.shinhan_caching_server.http.util.alias

class CreateVideoOption private constructor() {
    companion object {
        const val CREATE_REQUEST_RETRY_MAX_ATTEMPTS = 5L
        const val CREATE_REQUEST_RETRY_MIN_BACKOFF = 3L
    }
}
