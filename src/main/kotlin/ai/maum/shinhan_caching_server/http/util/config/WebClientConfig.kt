package ai.maum.shinhan_caching_server.http.util.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.function.client.WebClient

@Configuration
class WebClientConfig(
    @Value("\${ai.maum.shinhan.management_api_server.address}")
    private val apiServerUrl: String,
    @Value("\${ai.maum.shinhan.video_processing_server.address}")
    private val videoProcessingServerUrl: String
) {
    @Bean
    fun apiServerWebClient() = WebClient.create(apiServerUrl)

    @Bean
    fun videoProcessingServerWebClient() = WebClient.create(videoProcessingServerUrl)
}
