package ai.maum.shinhan_caching_server.http.exception

class CreateTargetSizeZeroException : RuntimeException("생성할 수 있는 영상 없음")
