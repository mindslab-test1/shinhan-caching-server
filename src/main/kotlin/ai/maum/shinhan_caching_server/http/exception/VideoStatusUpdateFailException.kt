package ai.maum.shinhan_caching_server.http.exception

class VideoStatusUpdateFailException : RuntimeException("이미 생성된 영상 또는 존재하지 않는 영상")
