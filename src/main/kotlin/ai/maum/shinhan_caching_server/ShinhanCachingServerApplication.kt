package ai.maum.shinhan_caching_server

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ShinhanCachingServerApplication

fun main(args: Array<String>) {
    runApplication<ShinhanCachingServerApplication>(*args)
}
