package ai.maum.shinhan_caching_server.r2dbc.repository

import ai.maum.shinhan_caching_server.r2dbc.entity.Video
import org.springframework.data.r2dbc.repository.Modifying
import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.repository.reactive.ReactiveCrudRepository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface VideoRepository : ReactiveCrudRepository<Video, Int> {
    @Modifying
    @Query("update video set status = 2, updated = NOW() where file = :file and status = 1;")
    fun updateStatusCompleteByFile(file: String): Mono<Int>

    @Modifying
    @Query(
        "update video set status = 1, updated = NOW() where " +
            "file in(:files1) " +
            "and (select count(*) from video where file in(:files2) and status = 0) = :reqCnt"
    )
    fun updateStatusProgressByFiles(files1: List<String>, files2: List<String>, reqCnt: Int): Mono<Int>

    fun countByLiveYnAndStatus(liveYn: String, status: Int): Mono<Int>

    @Query("select * from video where live_yn = 'N' and status = 0 limit :targetSize;")
    fun findCreateTargets(targetSize: Int): Flux<Video>
}
