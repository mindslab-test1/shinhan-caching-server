package ai.maum.shinhan_caching_server.r2dbc.entity

import org.springframework.data.annotation.Id
import java.sql.Timestamp

class Video {
    @Id
    var id: Int? = null
    var avatar: String? = null
    var outfit: String? = null
    var background: String? = null
    var utter: String? = null
    var resolution: String? = null
    var width: Int? = null
    var height: Int? = null
    var gesture: String? = null
    var scenarioVersion: String? = null
    var ttsHost: String? = null
    var ttsPort: Int? = null
    var ttsPath: String? = null
    var w2lHost: String? = null
    var w2lPort: Int? = null
    var w2lPath: String? = null
    var liveYn: String? = null
    var file: String? = null
    var status: Int? = null
    var created: Timestamp? = null
    var updated: Timestamp? = null
}
